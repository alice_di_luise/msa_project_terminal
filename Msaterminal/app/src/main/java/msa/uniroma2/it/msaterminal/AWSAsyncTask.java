package msa.uniroma2.it.msaterminal;

import android.os.AsyncTask;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.SetTopicAttributesRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;

import java.util.ArrayList;

/**
 * Created by alicediluise on 16/05/16.
 */
public class AWSAsyncTask extends AsyncTask<String, Integer, Integer> {

    // these Strings / or String are / is the parameters of the task, that can be handed over via the excecute(params) method of AsyncTask
    protected Integer doInBackground(String... params) {

        String param1 = params[0];
        String param2 = params[1];
        // and so on...
        // do something with the parameters...
        // be careful, this can easily result in a ArrayIndexOutOfBounds exception
        // if you try to access more parameters than you handed over

        AWSCredentials credentials = new BasicAWSCredentials("AKIAINS54N52RUZ7R3IA", "dnN+ZMaIooJNlUU8AUZPKBkipLrVgEFCaqV5MdjP");
        AmazonSNSClient snsClient = new AmazonSNSClient( credentials );
        AmazonSQSClient sqsClient = new AmazonSQSClient( credentials );

       // snsClient.setEndpoint("https://sqs.eu-west-1.amazonaws.com");
        snsClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
        sqsClient.setRegion(Region.getRegion(Regions.EU_WEST_1));

        CreateTopicRequest ctr = new CreateTopicRequest( "ThirdTopic" );
        CreateTopicResult result = snsClient.createTopic(ctr);

        SetTopicAttributesRequest tar = new SetTopicAttributesRequest( result.getTopicArn(), "DisplayName", "MessageBoard" );
        snsClient.setTopicAttributes( tar );

        //Create queue
        CreateQueueRequest cqr = new CreateQueueRequest( "ThirdQueue" );
        CreateQueueResult resultQueue = sqsClient.createQueue( cqr );

        //Subscribe sqs queue to sns topic
        ArrayList<String> attributes = new ArrayList<String>();
        attributes.add("QueueArn");

        ArrayList<String> permissions = new ArrayList<String>();
        permissions.add("SendMessage");

        ArrayList<String> awsAccounts = new ArrayList<String>();
//        awsAccounts.add("Everybody(*)");

        GetQueueAttributesResult queueArn = sqsClient.getQueueAttributes(resultQueue.getQueueUrl(),attributes);

//        sqsClient.addPermission(resultQueue.getQueueUrl(), "permission", awsAccounts , permissions);

        SubscribeRequest request = new SubscribeRequest();
        request.withEndpoint(queueArn.getAttributes().get("QueueArn")).withProtocol( "sqs" ).withTopicArn( result.getTopicArn() );

        snsClient.subscribe(request);

        // do something here with params
        // the params could for example contain an url and you could download stuff using this url here

        // the Integer variable is used for progress
        //publishProgress(someInt);

        // once the data is downloaded (for example JSON data)
        // parse the data and return it to the onPostExecute() method
        // in this example the return data is simply a long value
        // this could also be a list of your custom-objects, ...
        return 1;
    }

    // this is called whenever you call puhlishProgress(Integer), for example when updating a progressbar when downloading stuff
    protected void onProgressUpdate(Integer... progress) {
       // setProgressPercent(progress[0]);
    }

    // the onPostexecute method receives the return type of doInBackGround()
   // protected void onPostExecute(Long result) {
        // do something with the result, for example display the received Data in a ListView
        // in this case, "result" would contain the "someLong" variable returned by doInBackground();
   // }
}
